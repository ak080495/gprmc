import unittest

from gprmc.convert import divide_by_three

# from gprmc2.gprmc_a import divide_by_three

class TestDivideByThree(unittest.TestCase):

	def test_divide_by_three(self):
		self.assertEqual(divide_by_three(12), 4)

unittest.main()